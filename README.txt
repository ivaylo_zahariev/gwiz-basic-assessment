Hi David

I hope you are doing well :)

Just to let you know.. It is the first time I use Gravity Forms. I found an issue with passing the form id in the method `delete_child_entries_on_save_and_continue`.

I hope this was the only bug.

Please let me know if you need anything else!
Ivaylo
